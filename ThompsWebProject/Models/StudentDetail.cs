﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThompsWebApps.Models
{
	public class StudentDetail
	{
		public int ID { get; set; }
		public string StudentName { get; set; }
		public DateTime EnrollmentDate { get; set; }
		public string Course { get; set; }
		public string Grade { get; set; }
		public string Credit { get; set; }
	}
}